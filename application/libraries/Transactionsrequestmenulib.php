<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactionsrequestmenulib {

    protected $params;
    protected $table;
    protected $CI;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = $params;
        $this->table = "t_transaction_request_menu";
        $this->CI =& get_instance();
    }

    public function create($data)
    {
        return $this->params["sql"]->create(
            $data, $this->table
        );
    }

    public function update($where, $data)
    {
        return $this->params["sql"]->update(
            $where, $data, $this->table
        );
    }

    public function delete($where)
    {
        return $this->params["sql"]->delete($where, $this->table);
    }

    public function filter($where, $search, $page, $orderDirection)
    {
        // Preparing Filter
        $limit = 12;
        $offset = ($page * $limit);

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_transaction_request_menu`.* FROM `t_transaction_request_menu`
            WHERE $where
            ORDER BY `t_transaction_request_menu`.`created_at` $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        // Return Response
        return $data;
    }

    public function all($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_transaction_request_menu`.* FROM `t_transaction_request_menu`
            WHERE $where
            ORDER BY `t_transaction_request_menu`.`created_at` DESC
        ")->result_array();

        // Return Response
        return $data;
    }

    public function get($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_transaction_request_menu`.* FROM `t_transaction_request_menu`
            WHERE $where
        ")->result_array();

        if (count($data) != 1)
            return null;
    
        $item = $data[0];

        return $item;
    }

    public function size($where, $search, $orderDirection)
    {
        // Load Icon By Filter
        return $this->params["sql"]->query("
            SELECT count(`t_transaction_request_menu`.`id`) as `total` FROM `t_transaction_request_menu`
            WHERE $where
            ORDER BY `t_transaction_request_menu`.`created_at` $orderDirection
        ")->row()->total;
    }

}
