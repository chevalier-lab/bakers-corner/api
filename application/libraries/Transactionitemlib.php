<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactionitemlib {

    protected $params;
    protected $table;
    protected $CI;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = $params;
        $this->table = "t_transaction_product_item";
        $this->CI =& get_instance();

        $this->CI->load->library("productslib", $params);
    }

    public function create($data)
    {
        return $this->params["sql"]->create(
            $data, $this->table
        );
    }

    public function update($where, $data)
    {
        return $this->params["sql"]->update(
            $where, $data, $this->table
        );
    }

    public function delete($where)
    {
        return $this->params["sql"]->delete($where, $this->table);
    }

    public function filter($where, $search, $page, $orderDirection)
    {
        // Preparing Filter
        $limit = 12;
        $offset = ($page * $limit);

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_transaction_product_item`.* FROM `t_transaction_product_item`
            WHERE $where
            ORDER BY `t_transaction_product_item`.`created_at` $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        // Create Response
        $response = array();

        foreach ($data as $item) {
            $temp = $this->CI->productslib->get("`t_products`.`id` = " . $item['id_t_products']);
            if (!empty($temp)) {
                unset($temp["created_at"]);
                unset($temp["updated_at"]);
            }

            $item["product"] = $temp;
            unset($item["id_t_products"]);
            unset($item["id_m_users"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function all($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_transaction_product_item`.* FROM `t_transaction_product_item`
            WHERE $where
            ORDER BY `t_transaction_product_item`.`created_at` DESC
        ")->result_array();

        // Create Response
        $response = array();

        foreach ($data as $item) {
            $temp = $this->CI->productslib->get("`t_products`.`id` = " . $item['id_t_products']);
            if (!empty($temp)) {
                unset($temp["created_at"]);
                unset($temp["updated_at"]);
            }

            $item["product"] = $temp;
            unset($item["id_t_products"]);
            unset($item["id_m_users"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function get($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_transaction_product_item`.* FROM `t_transaction_product_item`
            WHERE $where
        ")->result_array();

        if (count($data) != 1)
            return null;
    
        $item = $data[0];
        
        // Load Media
        $temp = $this->CI->productslib->get("`t_products`.`id` = " . $item['id_t_products']);
        if (!empty($temp)) {
            unset($temp["created_at"]);
            unset($temp["updated_at"]);
        }

        $item["product"] = $temp;
        unset($item["id_t_products"]);
        unset($item["id_m_users"]);

        return $item;
    }

    public function get_once($where, $search)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_transaction_product_item`.* FROM `t_transaction_product_item`
            JOIN `t_products` ON `t_products`.`id` = `t_transaction_product_item`.`id_t_products`
            WHERE $where AND 
            (`t_products`.`product_name` LIKE '%$search%' OR `t_products`.`product_description` LIKE '%$search%' OR
            `t_products`.`product_price` LIKE '%$search%')
        ")->result_array();

        if (count($data) == 0)
            return null;
    
        $item = $data[0];
        
        // Load Media
        $temp = $this->CI->productslib->get("`t_products`.`id` = " . $item['id_t_products']);
        if (!empty($temp)) {
            unset($temp["created_at"]);
            unset($temp["updated_at"]);
        }

        $item["product"] = $temp;
        unset($item["id_t_products"]);
        unset($item["id_m_users"]);

        return $item;
    }

    public function size($where, $search, $orderDirection)
    {
        // Load Icon By Filter
        return $this->params["sql"]->query("
            SELECT count(`t_transaction_product_item`.`id`) as `total` FROM `t_transaction_product_item`
            WHERE $where
            ORDER BY `t_transaction_product_item`.`created_at` $orderDirection
        ")->row()->total;
    }

}
