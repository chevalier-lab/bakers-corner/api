<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedbackslib {

    protected $params;
    protected $table;
    protected $CI;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = $params;
        $this->table = "t_feedback_product";
        $this->CI =& get_instance();

        $this->CI->load->library("master-data/userslib", $params);
        $this->CI->load->library("productslib", $params);
    }

    public function create($data)
    {
        return $this->params["sql"]->create(
            $data, $this->table
        );
    }

    public function filter($id, $search, $page, $orderDirection)
    {
        // Preparing Filter
        $limit = 12;
        $offset = ($page * $limit);

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_feedback_product`.* FROM `t_feedback_product`
            WHERE (`t_feedback_product`.`rating` LIKE '%".$search."%' OR `t_feedback_product`.`comment` LIKE '%".$search."%')
            AND `t_feedback_product`.`is_visible` = 1
            AND `t_feedback_product`.`id_t_products` = $id
            ORDER BY `t_feedback_product`.`created_at` $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        $response = array();

        foreach ($data as $item) {
            $temp = $this->CI->userslib->get("`m_users`.`id` = " . $item['id_m_users']);
            if (!empty($temp)) {
                unset($temp["id"]);
                unset($temp["token"]);
                unset($temp["password"]);
                unset($temp["created_at"]);
                unset($temp["updated_at"]);
            }

            $item["user"] = $temp;
            unset($item["id_m_users"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function my($id_m_users)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_feedback_product`.* FROM `t_feedback_product`
            WHERE
            `t_feedback_product`.`is_visible` = 1
            AND `t_feedback_product`.`id_m_users` = $id_m_users 
            ORDER BY `t_feedback_product`.`created_at` DESC
        ")->result_array();

        $response = array();

        foreach ($data as $item) {
            $temp = $this->CI->userslib->get("`m_users`.`id` = " . $item['id_m_users']);
            $tempProduct = $this->CI->productslib->get("`t_products`.`id` = " . $item["id_t_products"]);
            if (!empty($temp)) {
                unset($temp["id"]);
                unset($temp["token"]);
                unset($temp["password"]);
                unset($temp["created_at"]);
                unset($temp["updated_at"]);
            }

            $item["user"] = $temp;
            $item["product"] = $tempProduct;
            unset($item["id_m_users"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function my_filter($id_m_users, $search, $page, $orderDirection)
    {
        $limit = 12;
        $offset = ($page * $limit);

        $orderBy = "`t_feedback_product`.`created_at`";

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_feedback_product`.* FROM `t_feedback_product`
            WHERE
            `t_feedback_product`.`is_visible` = 1
            AND `t_feedback_product`.`id_m_users` = $id_m_users 
            AND (`t_feedback_product`.`comment` LIKE '%$search%' OR `t_feedback_product`.`rating` LIKE '%$search%')
            ORDER BY $orderBy $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        $response = array();

        foreach ($data as $item) {
            $temp = $this->CI->userslib->get("`m_users`.`id` = " . $item['id_m_users']);
            $tempProduct = $this->CI->productslib->get("`t_products`.`id` = " . $item["id_t_products"]);
            if (!empty($temp)) {
                unset($temp["id"]);
                unset($temp["token"]);
                unset($temp["password"]);
                unset($temp["created_at"]);
                unset($temp["updated_at"]);
            }

            $item["user"] = $temp;
            $item["product"] = $tempProduct;
            unset($item["id_m_users"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function detail($id_m_users, $id)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_feedback_product`.* FROM `t_feedback_product`
            WHERE
            `t_feedback_product`.`is_visible` = 1
            AND `t_feedback_product`.`id_m_users` = $id_m_users 
            AND `t_feedback_product`.`id` = $id
            ORDER BY `t_feedback_product`.`created_at` DESC
        ")->result_array();

        $response = array();

        foreach ($data as $item) {
            $temp = $this->CI->userslib->get("`m_users`.`id` = " . $item['id_m_users']);
            if (!empty($temp)) {
                unset($temp["id"]);
                unset($temp["token"]);
                unset($temp["password"]);
                unset($temp["created_at"]);
                unset($temp["updated_at"]);
            }

            $item["user"] = $temp;
            unset($item["id_m_users"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function size($id, $search, $orderDirection)
    {
        // Load Icon By Filter
        return $this->params["sql"]->query("
            SELECT count(`t_feedback_product`.`id`) as `total` FROM `t_feedback_product`
            WHERE (`t_feedback_product`.`rating` LIKE '%".$search."%' OR `t_feedback_product`.`comment` LIKE '%".$search."%')
            AND `t_feedback_product`.`is_visible` = 1
            AND `t_feedback_product`.`id_t_products` = $id
            ORDER BY `t_feedback_product`.`created_at` $orderDirection
        ")->row()->total;
    }

    public function size_my($id_m_users)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT COUNT(`t_feedback_product`.`id`) as `total` FROM `t_feedback_product`
            WHERE
            `t_feedback_product`.`is_visible` = 1
            AND `t_feedback_product`.`id_m_users` = $id_m_users 
            ORDER BY `t_feedback_product`.`created_at` DESC
        ")->row()->total;

        // Return Response
        return $data;
    }

    public function size_my_filter($id_m_users, $search)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT COUNT(`t_feedback_product`.`id`) as `total` FROM `t_feedback_product`
            WHERE
            `t_feedback_product`.`is_visible` = 1
            AND `t_feedback_product`.`id_m_users` = $id_m_users 
            AND (`t_feedback_product`.`comment` LIKE '%$search%' OR `t_feedback_product`.`rating` LIKE '%$search%')
        ")->row()->total;

        // Return Response
        return $data;
    }

}
