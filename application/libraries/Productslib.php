<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Productslib {

    protected $params;
    protected $table;
    protected $CI;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = $params;
        $this->table = "t_products";
        $this->CI =& get_instance();

        $this->CI->load->library("master-data/mediaslib", $params);
        $this->CI->load->library("master-data/categorieslib", $params);
    }

    public function filter($is_price_order, $is_discount_order, $search, $page, $orderDirection)
    {
        // Preparing Filter
        $limit = 12;
        $offset = ($page * $limit);

        $orderBy = "`t_products`.`created_at`";
        $whereAnd = "";

        if ($is_price_order) $orderBy = "`t_products`.`product_price`";
        if ($is_discount_order) $whereAnd = "AND `t_products`.`discount` != 0";

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_products`.* FROM `t_products`
            WHERE (`t_products`.`product_name` LIKE '%".$search."%' OR `t_products`.`product_description` LIKE '%".$search."%'
            OR `t_products`.`product_price` LIKE '%".$search."%'
            OR `t_products`.`discount` LIKE '%".$search."%')
            $whereAnd
            ORDER BY $orderBy $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        $response = array();

        foreach ($data as $item) {
            $category = $this->CI->categorieslib->get("`m_categories`.`id` = " . $item['id_m_categories']);
            if (!empty($category)) {
                unset($category["id"]);
                unset($category["created_at"]);
                unset($category["updated_at"]);
            }

            $media = $this->CI->mediaslib->get("`m_medias`.`id` = " . $item['id_m_medias']);
            if (!empty($media)) {
                unset($media["id"]);
                unset($media["created_at"]);
                unset($media["updated_at"]);
            }

            $item["category"] = $category;
            $item["media"] = $media;
            unset($item["id_m_categories"]);
            unset($item["id_m_medias"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function filter_by_category($is_price_order, $is_discount_order, $categoryID, $search, $page, $orderDirection)
    {
        // Preparing Filter
        $limit = 12;
        $offset = ($page * $limit);

        $orderBy = "`t_products`.`created_at`";
        $whereAnd = "";

        if ($is_price_order) $orderBy = "`t_products`.`product_price`";
        if ($is_discount_order) $whereAnd = "AND `t_products`.`discount` != 0";

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_products`.* FROM `t_products`
            WHERE (`t_products`.`product_name` LIKE '%".$search."%' OR `t_products`.`product_description` LIKE '%".$search."%'
            OR `t_products`.`product_price` LIKE '%".$search."%'
            OR `t_products`.`discount` LIKE '%".$search."%')
            AND `t_products`.`id_m_categories` = $categoryID
            $whereAnd
            ORDER BY $orderBy $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        $response = array();

        foreach ($data as $item) {
            $category = $this->CI->categorieslib->get("`m_categories`.`id` = " . $item['id_m_categories']);
            if (!empty($category)) {
                unset($category["id"]);
                unset($category["created_at"]);
                unset($category["updated_at"]);
            }

            $media = $this->CI->mediaslib->get("`m_medias`.`id` = " . $item['id_m_medias']);
            if (!empty($media)) {
                unset($media["id"]);
                unset($media["created_at"]);
                unset($media["updated_at"]);
            }

            $item["category"] = $category;
            $item["media"] = $media;
            unset($item["id_m_categories"]);
            unset($item["id_m_medias"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function recomended($is_price_order, $is_discount_order, $search, $page, $orderDirection)
    {
        // Preparing Filter
        $limit = 12;
        $offset = ($page * $limit);

        $orderBy = "`t_products`.`created_at`";
        $whereAnd = "";

        if ($is_price_order) $orderBy = "`t_products`.`product_price`";
        if ($is_discount_order) $whereAnd = "AND `t_products`.`discount` != 0";

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_products`.* FROM `t_products`
            WHERE (`t_products`.`product_name` LIKE '%".$search."%' OR `t_products`.`product_description` LIKE '%".$search."%'
            OR `t_products`.`product_price` LIKE '%".$search."%'
            OR `t_products`.`discount` LIKE '%".$search."%')
            AND `t_products`.`is_visible` = 1
            AND `t_products`.`is_recommended` = 1
            $whereAnd
            ORDER BY $orderBy $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        $response = array();

        foreach ($data as $item) {
            $category = $this->CI->categorieslib->get("`m_categories`.`id` = " . $item['id_m_categories']);
            if (!empty($category)) {
                unset($category["id"]);
                unset($category["created_at"]);
                unset($category["updated_at"]);
            }

            $media = $this->CI->mediaslib->get("`m_medias`.`id` = " . $item['id_m_medias']);
            if (!empty($media)) {
                unset($media["id"]);
                unset($media["created_at"]);
                unset($media["updated_at"]);
            }

            $item["category"] = $category;
            $item["media"] = $media;
            unset($item["id_m_categories"]);
            unset($item["id_m_medias"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function recomended_by_category($is_price_order, $is_discount_order, $categoryID, $search, $page, $orderDirection)
    {
        // Preparing Filter
        $limit = 12;
        $offset = ($page * $limit);

        $orderBy = "`t_products`.`created_at`";
        $whereAnd = "";

        if ($is_price_order) $orderBy = "`t_products`.`product_price`";
        if ($is_discount_order) $whereAnd = "AND `t_products`.`discount` != 0";

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_products`.* FROM `t_products`
            WHERE (`t_products`.`product_name` LIKE '%".$search."%' OR `t_products`.`product_description` LIKE '%".$search."%'
            OR `t_products`.`product_price` LIKE '%".$search."%'
            OR `t_products`.`discount` LIKE '%".$search."%')
            AND `t_products`.`is_visible` = 1
            AND `t_products`.`is_recommended` = 1
            AND `t_products`.`id_m_categories` = $categoryID
            $whereAnd
            ORDER BY $orderBy $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        $response = array();

        foreach ($data as $item) {
            $category = $this->CI->categorieslib->get("`m_categories`.`id` = " . $item['id_m_categories']);
            if (!empty($category)) {
                unset($category["id"]);
                unset($category["created_at"]);
                unset($category["updated_at"]);
            }

            $media = $this->CI->mediaslib->get("`m_medias`.`id` = " . $item['id_m_medias']);
            if (!empty($media)) {
                unset($media["id"]);
                unset($media["created_at"]);
                unset($media["updated_at"]);
            }

            $item["category"] = $category;
            $item["media"] = $media;
            unset($item["id_m_categories"]);
            unset($item["id_m_medias"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function get($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_products`.* FROM `t_products`
            WHERE $where
        ")->result_array();

        if (count($data) != 1)
            return null;
    
        $item = $data[0];

        $category = $this->CI->categorieslib->get("`m_categories`.`id` = " . $item['id_m_categories']);
        if (!empty($category)) {
            unset($category["created_at"]);
            unset($category["updated_at"]);
        }

        $media = $this->CI->mediaslib->get("`m_medias`.`id` = " . $item['id_m_medias']);
        if (!empty($media)) {
            unset($media["created_at"]);
            unset($media["updated_at"]);
        }

        $u_product_photos = $this->params["sql"]->query("
            SELECT `m_medias`.`url`, `m_medias`.`file_name` FROM `u_product_photos`
            JOIN `m_medias` ON `m_medias`.`id` = `u_product_photos`.`id_m_medias`
            WHERE `u_product_photos`.`id_t_products` = ".$item["id"]."
        ")->result_array();

        $item["category"] = $category;
        $item["media"] = $media;
        unset($item["id_m_categories"]);
        unset($item["id_m_medias"]);
        $item["photos"] = $u_product_photos;

        return $item;
    }

    public function size($search, $orderDirection)
    {
        // Load Icon By Filter
        return $this->params["sql"]->query("
            SELECT count(`t_products`.`id`) as `total` FROM `t_products`
            WHERE (`t_products`.`product_name` LIKE '%".$search."%' OR `t_products`.`product_description` LIKE '%".$search."%'
            OR `t_products`.`product_price` LIKE '%".$search."%'
            OR `t_products`.`discount` LIKE '%".$search."%')
            ORDER BY `t_products`.`created_at` $orderDirection
        ")->row()->total;
    }

    public function size_by_category($is_price_order, $is_discount_order, $categoryID, $search)
    {
        // Preparing Filter
        $orderBy = "`t_products`.`created_at`";
        $whereAnd = "";

        if ($is_price_order) $orderBy = "`t_products`.`product_price`";
        if ($is_discount_order) $whereAnd = "AND `t_products`.`discount` != 0";

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT COUNT(`t_products`.`id`) as `total` FROM `t_products`
            WHERE (`t_products`.`product_name` LIKE '%".$search."%' OR `t_products`.`product_description` LIKE '%".$search."%'
            OR `t_products`.`product_price` LIKE '%".$search."%'
            OR `t_products`.`discount` LIKE '%".$search."%')
            AND `t_products`.`is_visible` = 1
            AND `t_products`.`is_recommended` = 1
            AND `t_products`.`id_m_categories` = $categoryID
            $whereAnd
        ")->row()->total;

        // Return Response
        return $data;
    }

    public function size_filter($is_price_order, $is_discount_order, $search)
    {
        $orderBy = "`t_products`.`created_at`";
        $whereAnd = "";

        if ($is_price_order) $orderBy = "`t_products`.`product_price`";
        if ($is_discount_order) $whereAnd = "AND `t_products`.`discount` != 0";

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT COUNT(`t_products`.`id`) as `total` FROM `t_products`
            WHERE (`t_products`.`product_name` LIKE '%".$search."%' OR `t_products`.`product_description` LIKE '%".$search."%'
            OR `t_products`.`product_price` LIKE '%".$search."%'
            OR `t_products`.`discount` LIKE '%".$search."%')
            $whereAnd
        ")->row()->total;

        // Return Response
        return $data;
    }

    public function size_recomended($is_price_order, $is_discount_order, $search)
    {
        // Preparing Filter
        $orderBy = "`t_products`.`created_at`";
        $whereAnd = "";

        if ($is_price_order) $orderBy = "`t_products`.`product_price`";
        if ($is_discount_order) $whereAnd = "AND `t_products`.`discount` != 0";

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT COUNT(`t_products`.`id`) as `total` FROM `t_products`
            WHERE (`t_products`.`product_name` LIKE '%".$search."%' OR `t_products`.`product_description` LIKE '%".$search."%'
            OR `t_products`.`product_price` LIKE '%".$search."%'
            OR `t_products`.`discount` LIKE '%".$search."%')
            AND `t_products`.`is_visible` = 1
            AND `t_products`.`is_recommended` = 1
            $whereAnd
        ")->row()->total;

        // Return Response
        return $data;
    }

}
