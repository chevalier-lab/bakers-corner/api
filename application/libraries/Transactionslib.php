<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactionslib {

    protected $params;
    protected $table;
    protected $CI;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = $params;
        $this->table = "t_transaction_product";
        $this->CI =& get_instance();
    }

    public function create($data)
    {
        return $this->params["sql"]->create(
            $data, $this->table
        );
    }

    public function update($where, $data)
    {
        return $this->params["sql"]->update(
            $where, $data, $this->table
        );
    }

    public function delete($where)
    {
        return $this->params["sql"]->delete($where, $this->table);
    }

    public function filter($where, $search, $page, $orderDirection)
    {
        // Preparing Filter
        $limit = 12;
        $offset = ($page * $limit);

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_transaction_product`.* FROM `t_transaction_product`
            WHERE $where 
            ORDER BY `t_transaction_product`.`created_at` $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        // Return Response
        return $data;
    }

    public function all($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_transaction_product`.* FROM `t_transaction_product`
            WHERE $where
            ORDER BY `t_transaction_product`.`created_at` DESC
        ")->result_array();

        // Return Response
        return $data;
    }

    public function get($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_transaction_product`.* FROM `t_transaction_product`
            WHERE $where
        ")->result_array();

        if (count($data) != 1)
            return null;
    
        $item = $data[0];

        return $item;
    }

    public function feedbackSize($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT COUNT(`t_transaction_product`.`id`) as `total` FROM `t_transaction_product`
            WHERE $where
        ")->row()->total;

        return $data;
    }

    public function size($where, $search, $orderDirection)
    {
        // Load Icon By Filter
        return $this->params["sql"]->query("
            SELECT count(`t_transaction_product`.`id`) as `total` FROM `t_transaction_product`
            WHERE $where
            ORDER BY `t_transaction_product`.`created_at` $orderDirection
        ")->row()->total;
    }

}
