<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Promoandbannerslib {

    protected $params;
    protected $table;
    protected $CI;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = $params;
        $this->table = "t_banner";
        $this->CI =& get_instance();

        $this->CI->load->library("master-data/mediaslib", $params);
    }

    public function filter($search, $page, $orderDirection)
    {
        // Preparing Filter
        $limit = 12;
        $offset = ($page * $limit);

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_banner`.* FROM `t_banner`
            WHERE (`t_banner`.`title` LIKE '%".$search."%' OR `t_banner`.`content` LIKE '%".$search."%')
            AND `t_banner`.`is_visible` = 1
            ORDER BY `t_banner`.`created_at` $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        // Create Response
        $response = array();

        foreach ($data as $item) {
            $temp = $this->CI->mediaslib->get("`m_medias`.`id` = " . $item['id_m_medias']);
            if (!empty($temp)) {
                unset($temp["id"]);
                unset($temp["created_at"]);
                unset($temp["updated_at"]);
            }

            $item["media"] = $temp;
            unset($item["id_m_medias"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function get($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_banner`.* FROM `t_banner`
            WHERE $where
        ")->result_array();

        if (count($data) != 1)
            return null;
    
        $item = $data[0];
        
        // Load Media
        $temp = $this->CI->mediaslib->get("`m_medias`.`id` = " . $item['id_m_medias']);
        if (!empty($temp)) {
            unset($temp["id"]);
            unset($temp["created_at"]);
            unset($temp["updated_at"]);
        }

        $item["media"] = $temp;
        unset($item["id_m_medias"]);

        return $item;
    }

    public function size($search, $orderDirection)
    {
        // Load Icon By Filter
        return $this->params["sql"]->query("
            SELECT count(`t_banner`.`id`) as `total` FROM `t_banner`
            WHERE (`t_banner`.`title` LIKE '%".$search."%' OR `t_banner`.`content` LIKE '%".$search."%')
            AND `t_banner`.`is_visible` = 1
            ORDER BY `t_banner`.`created_at` $orderDirection
        ")->row()->total;
    }

}
