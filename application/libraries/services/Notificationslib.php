<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificationslib {

    protected $params;
    protected $table;
    protected $CI;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = $params;
        $this->table = "t_notification";
        $this->CI =& get_instance();
    }

    public function create($data)
    {
        return $this->params["sql"]->create(
            $data, $this->table
        );
    }

    public function filter($where, $search, $page, $orderDirection)
    {
        // Preparing Filter
        $limit = 12;
        $offset = ($page * $limit);

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_notification`.* FROM `t_notification`
            WHERE (`t_notification`.`title` LIKE '%".$search."%' OR `t_notification`.`content` LIKE '%".$search."%')
            AND $where
            ORDER BY `t_notification`.`created_at` $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        // Create Response
        $response = array();

        foreach ($data as $item) {
            unset($item["id_m_users"]);
            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function get($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_notification`.* FROM `t_notification`
            WHERE $where
        ")->result_array();

        if (count($data) != 1)
            return null;
    
        $item = $data[0];
        unset($item["id_m_users"]);

        return $item;
    }

    public function size($where, $search, $orderDirection)
    {
        // Load Icon By Filter
        return $this->params["sql"]->query("
            SELECT count(`t_notification`.`id`) as `total` FROM `t_notification`
            WHERE (`t_notification`.`title` LIKE '%".$search."%' OR `t_notification`.`content` LIKE '%".$search."%')
            AND $where
            ORDER BY `t_notification`.`created_at` $orderDirection
        ")->row()->total;
    }

}
