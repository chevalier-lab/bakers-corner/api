<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Announcementslib {

    protected $params;
    protected $table;
    protected $CI;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = $params;
        $this->table = "t_announcement";
        $this->CI =& get_instance();
    }

    public function filter($search, $page, $orderDirection)
    {
        // Preparing Filter
        $limit = 12;
        $offset = ($page * $limit);

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_announcement`.* FROM `t_announcement`
            WHERE (`t_announcement`.`title` LIKE '%".$search."%' OR `t_announcement`.`content` LIKE '%".$search."%')
            ORDER BY `t_announcement`.`created_at` $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        // Return Response
        return $data;
    }

    public function get($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_announcement`.* FROM `t_announcement`
            WHERE $where
        ")->result_array();

        if (count($data) != 1)
            return null;
    
        $item = $data[0];

        return $item;
    }

    public function size($search, $orderDirection)
    {
        // Load Icon By Filter
        return $this->params["sql"]->query("
            SELECT count(`t_announcement`.`id`) as `total` FROM `t_announcement`
            WHERE (`t_announcement`.`title` LIKE '%".$search."%' OR `t_announcement`.`content` LIKE '%".$search."%')
            ORDER BY `t_announcement`.`created_at` $orderDirection
        ")->row()->total;
    }

}
