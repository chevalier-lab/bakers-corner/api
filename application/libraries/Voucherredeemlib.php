<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucherredeemlib {

    protected $params;
    protected $table;
    protected $CI;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = $params;
        $this->table = "t_voucher_redeem_user";
        $this->CI =& get_instance();

        $this->CI->load->library("voucherslib", $params);
    }

    public function create($data)
    {
        return $this->params["sql"]->create(
            $data, $this->table
        );
    }

    public function get($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_voucher_redeem_user`.* FROM `t_voucher_redeem_user`
            WHERE $where
        ")->result_array();

        if (count($data) != 1)
            return null;
    
        $item = $data[0];

        $temp = $this->CI->voucherslib->get("`t_voucher`.`id` = " . $item['id_t_voucher']);        

        return $temp;
    }

}
