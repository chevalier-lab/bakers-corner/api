<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Settingslib {

    protected $params;
    protected $table;
    protected $CI;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = $params;
        $this->table = "m_settings";
        $this->CI =& get_instance();

        $this->CI->load->library("master-data/mediaslib", $params);
    }

    public function update($data)
    {
        return $this->params["sql"]->update(
            array(), $data, $this->table
        );
    }

    public function get()
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `m_settings`.* FROM `m_settings`
        ")->result_array();

        if (count($data) != 1)
            return null;
    
        $item = $data[0];
        
        // Load Media
        $temp = $this->CI->mediaslib->get("`m_medias`.`id` = " . $item['id_m_medias']);
        if (!empty($temp)) {
            unset($temp["id"]);
            unset($temp["created_at"]);
            unset($temp["updated_at"]);
        }

        $item["media"] = $temp;
        unset($item["id_m_medias"]);
        unset($item["id"]);

        return $item;
    }

}
