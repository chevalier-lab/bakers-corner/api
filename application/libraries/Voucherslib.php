<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucherslib {

    protected $params;
    protected $table;
    protected $CI;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = $params;
        $this->table = "t_voucher";
        $this->CI =& get_instance();

        $this->CI->load->library("master-data/mediaslib", $params);
    }

    public function create($data)
    {
        return $this->params["sql"]->create(
            $data, $this->table
        );
    }

    public function update($where, $data)
    {
        return $this->params["sql"]->update(
            $where, $data, $this->table
        );
    }

    public function delete($where)
    {
        return $this->params["sql"]->delete($where, $this->table);
    }

    public function filter($where, $search, $page, $orderDirection)
    {
        // Preparing Filter
        $limit = 12;
        $offset = ($page * $limit);

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_voucher`.* FROM `t_voucher`
            WHERE $where
            ORDER BY `t_voucher`.`created_at` $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        // Create Response
        $response = array();

        foreach ($data as $item) {
            $temp = $this->CI->mediaslib->get("`m_medias`.`id` = " . $item['id_m_medias']);
            if (!empty($temp)) {
                unset($temp["id"]);
                unset($temp["created_at"]);
                unset($temp["updated_at"]);
            }

            $item["media"] = $temp;
            unset($item["id_m_medias"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function all($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_voucher`.* FROM `t_voucher`
            WHERE $where
            ORDER BY `t_voucher`.`created_at` DESC
        ")->result_array();

        // Create Response
        $response = array();

        foreach ($data as $item) {
            $temp = $this->CI->mediaslib->get("`m_medias`.`id` = " . $item['id_m_medias']);
            if (!empty($temp)) {
                unset($temp["id"]);
                unset($temp["created_at"]);
                unset($temp["updated_at"]);
            }

            $item["media"] = $temp;
            unset($item["id_m_medias"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function get($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `t_voucher`.* FROM `t_voucher`
            WHERE $where
        ")->result_array();

        if (count($data) != 1)
            return null;
    
        $item = $data[0];

        if ($item["id_m_medias"] != null) {
            $temp = $this->CI->mediaslib->get("`m_medias`.`id` = " . $item['id_m_medias']);
            if (!empty($temp)) {
                unset($temp["id"]);
                unset($temp["created_at"]);
                unset($temp["updated_at"]);
            } else $temp = null;
        } else $temp = null;

        $item["media"] = $temp;
        unset($item["id_m_medias"]);

        return $item;
    }

    public function size($where, $search, $orderDirection)
    {
        // Load Icon By Filter
        return $this->params["sql"]->query("
            SELECT count(`t_voucher`.`id`) as `total` FROM `t_voucher`
            WHERE $where
            ORDER BY `t_voucher`.`created_at` $orderDirection
        ")->row()->total;
    }

}
