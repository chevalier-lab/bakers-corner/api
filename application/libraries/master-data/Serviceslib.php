<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Serviceslib {

    protected $params;
    protected $table;
    protected $CI;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = $params;
        $this->table = "m_services";
        $this->CI =& get_instance();
    }

    public function all()
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `m_services`.* FROM `m_services`
        ")->result_array();

        // Return Response
        return $data;
    }

}
