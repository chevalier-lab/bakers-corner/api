<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorieslib {

    protected $params;
    protected $table;
    protected $CI;

    public function __construct($params)
    {
        // Do something with $params
        $this->params = $params;
        $this->table = "m_categories";
        $this->CI =& get_instance();

        $this->CI->load->library("master-data/iconslib", $params);
    }

    public function create($data)
    {
        return $this->params["sql"]->create(
            $data, $this->table
        );
    }

    public function update($where, $data)
    {
        return $this->params["sql"]->update(
            $where, $data, $this->table
        );
    }

    public function delete($where)
    {
        return $this->params["sql"]->delete($where, $this->table);
    }

    public function filter($search, $page, $orderDirection)
    {
        // Preparing Filter
        $limit = 12;
        $offset = ($page * $limit);

        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `m_categories`.* FROM `m_categories`
            WHERE `m_categories`.`category` LIKE '%".$search."%' OR `m_categories`.`slug` LIKE '%".$search."%'
            ORDER BY `m_categories`.`created_at` $orderDirection
            LIMIT $limit OFFSET $offset
        ")->result_array();

        // Create Response
        $response = array();

        foreach ($data as $item) {
            $temp = $this->CI->iconslib->get("`m_icons`.`id` = " . $item['id_m_icons']);
            if (!empty($temp)) {
                unset($temp["created_at"]);
                unset($temp["updated_at"]);
            }

            $item["icon"] = $temp;
            unset($item["id_m_icons"]);

            $response[] = $item;
        }

        // Return Response
        return $response;
    }

    public function get($where)
    {
        // Load Icon By Filter
        $data = $this->params["sql"]->query("
            SELECT `m_categories`.* FROM `m_categories`
            WHERE $where
        ")->result_array();

        if (count($data) != 1)
            return null;
    
        $item = $data[0];
        
        // Load icon
        $temp = $this->CI->iconslib->get("`m_icons`.`id` = " . $item['id_m_icons']);
        if (!empty($temp)) {
            unset($temp["created_at"]);
            unset($temp["updated_at"]);
        }

        $item["icon"] = $temp;
        unset($item["id_m_icons"]);

        return $item;
    }

    public function size($search, $orderDirection)
    {
        // Load Icon By Filter
        return $this->params["sql"]->query("
            SELECT count(`m_categories`.`id`) as `total` FROM `m_categories`
            WHERE `m_categories`.`category` LIKE '%".$search."%' OR `m_categories`.`slug` LIKE '%".$search."%'
            ORDER BY `m_categories`.`created_at` $orderDirection
        ")->row()->total;
    }

}
