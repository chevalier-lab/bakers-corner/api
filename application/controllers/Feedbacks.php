<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Feedbacks extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("customSQL");
        $this->load->model("request");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Init Request
        $this->request->init($this->custom_curl);

        // Load Library
        $this->load->library("feedbackslib", array(
            "sql" => $this->customSQL
        ));
        $this->load->library("productslib", array(
            "sql" => $this->customSQL
        ));
        $this->load->library("transactionslib", array(
            "sql" => $this->customSQL
        ));
    }

    public function index() 
    {
        $tempUser = $this->checkIsValid();
        $page = $this->input->get("page", TRUE) ?: 0;
        $search = $this->input->get("search", TRUE) ?: "";
        $orderDirection = $this->input->get("order-direction", TRUE) ?: "DESC";
        $is_all = $this->input->get("is-all", TRUE) ?: "no"; // yes atau no

        if ($is_all == "yes") {
            $data = $this->feedbackslib->my($tempUser["id"]);
            $size = $this->feedbackslib->size_my($tempUser["id"]);
        } else {
            $data = $this->feedbackslib->my_filter($tempUser["id"], $search, $page, $orderDirection);
            $size = $this->feedbackslib->size_my_filter($tempUser["id"], $search);
        }
        // unset($data["id"]);
        $this->request->res(200, $data, "Berhasil memuat data ulasan", array(
            "page" => $page,
            "search" => $search,
            "order-direction" => $orderDirection,
            "size" => array(
                "fetch" => count($data),
                "total" => $size
            )
        ));
    }

    public function detail($id) 
    {
        $tempUser = $this->checkIsValid();

        $data = $this->feedbackslib->detail($tempUser["id"], $id);
        if (count($data) == 1) {
            $data = $data[0];
            $data["products"] = $this->productslib->get("`t_products`.`id` = " . $data["id_t_products"]);
        }
        // unset($data["id"]);
        $this->request->res(200, $data, "Berhasil memuat data ulasan", null);
    }

    public function checkFeedback()
    {
        $tempUser = $this->checkIsValid();

        $data = $this->transactionslib->feedbackSize("`t_transaction_product`.`id_m_users` = " . $tempUser["id"] . " AND `t_transaction_product`.`is_already_feedback` = 0 AND `t_transaction_product`.`status` = 4");
        
        // unset($data["id"]);
        $this->request->res(200, $data, "Berhasil memuat data ulasan", null);
    }

    private function checkIsValid()
    {
        $tempUser = $this->customSQL->checkValid();
        if (count($tempUser) != 1)
            $this->request->res(403, null, "Tidak ter-otentikasi", null);
        $tempUser = $tempUser[0];
        return $tempUser;
    }

    private function checkID($checkID)
    {
        if ($checkID == -1)
            $this->request->res(500, null, "Terjadi kesalahan, silahkan cek masukan anda", null);
    }

}
