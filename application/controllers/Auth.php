<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("customSQL");
        $this->load->model("request");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Init Request
        $this->request->init($this->custom_curl);

        // Load Library
        $this->load->library("master-data/userslib", array(
            "sql" => $this->customSQL
        ));
        $this->load->library("services/notificationslib", array(
            "sql" => $this->customSQL
        ));
    }

    public function login() 
    {
        $email = $this->input->post_get("email", TRUE) ?: "";
        $password = $this->input->post_get("password", TRUE) ?: "";

        if (empty($email) || empty($password))
            $this->request->res(400, null, "Parameter tidak benar", null);

        $res = $this->userslib->get("`email` = '" . $email . "'");

        if (empty($res))
            $this->request->res(403, null, "Akun tidak ditemukan", null);

        if (!password_verify($password, $res["password"]))
            $this->request->res(403, null, "Password anda salah", null);    

        unset($res["password"]);

        $this->notificationslib->create(array(
            "type" => 0,
            "title" => "Berhasil melakukan otentikasi",
            "content" => "Berhasil melakukan otentikasi pada tanggal " . date("Y-m-d H:i:s"),
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
            "id_m_users" => $res["id"]
        ));

        // unset($res["id"]);
        
        $this->request->res(200, $res, "Berhasil melakukan otentikas", null);
    }

    public function registration()
    {
        $req = $this->request->raw();
        if (!isset($req["email"]) || empty($req["email"]) ||
            !isset($req["password"]) || empty($req["password"]) ||
            !isset($req["full_name"]) || empty($req["full_name"])) 
            $this->request->res(400, null, "Parameter tidak benar", null);

        $res = $this->userslib->get("`m_users`.`email` = '" . $req["email"] . "'");

        if (!empty($res))
            $this->request->res(500, null, "Akun sudah dibuat sebelumnya", null);

        $data = array(
            "email" => $req["email"],
            "full_name" => $req["full_name"],
            "password" => password_hash($req["password"], PASSWORD_DEFAULT),
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
            "id_m_medias" => 1,
            "level" => 0,
            "token" => md5(date_timestamp_get(date_create()) . $req["email"])
        );

        if (isset($req["phone_number"])) $data["phone_number"] = $req["phone_number"];
        if (isset($req["address"])) $data["address"] = $req["address"];

        $checkID = $this->userslib->create($data);
        $this->checkID($checkID);
        
        $data = $this->userslib->get("`m_users`.`id` = " . $checkID);

        $this->notificationslib->create(array(
            "type" => 0,
            "title" => "Berhasil melakukan registrasi",
            "content" => "Berhasil melakukan registrasi pada tanggal " . date("Y-m-d H:i:s"),
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
            "id_m_users" => $data["id"]
        ));

        unset($data["password"]);
        // unset($data["id"]);

        $this->request->res(200, $data, "Berhasil melakukan registrasi", null);
    }

    private function checkID($checkID)
    {
        if ($checkID == -1)
            $this->request->res(500, null, "Terjadi kesalahan, silahkan cek masukan anda", null);
    }

}
